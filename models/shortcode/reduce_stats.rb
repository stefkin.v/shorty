class Shortcode::ReduceStats
  def call
    DB.run <<-SQL
      with deleted as (delete from redirects returning shortcode_id, created_at),
           grouped as (select shortcode_id, count(*) as cnt, max(created_at) as lra from deleted group by shortcode_id)
      update shortcodes
      set redirects_count = redirects_count + grouped.cnt,
          last_redirect_at = grouped.lra
      from grouped
      where shortcodes.id = grouped.shortcode_id;
    SQL
  end
end
