class Shortcode::Stats
  include Dry::Transaction

  step :find
  map :calculate

  private

  def find(shortcode)
    model = Shortcode.find(shortcode: shortcode)

    if model
      Success(model)
    else
      Failure("not found")
    end
  end

  def calculate(shortcode)
    start_date = shortcode.created_at.utc.iso8601
    query = Shortcode::Stats::Query.new.call(shortcode)
    redirect_count = query[:count] + shortcode.redirects_count
    last_seen_date = (query[:max] || shortcode.last_redirect_at)&.utc&.iso8601

    { startDate: start_date,
      redirectCount: redirect_count,
      lastSeenDate: last_seen_date }.compact
  end
end
