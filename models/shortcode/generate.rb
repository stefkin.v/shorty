require 'digest'
require 'securerandom'
require 'base62-rb'

module Shortcode::Generate
  module_function

  LENGTH = 6
  MAX_BASE62 = 62**LENGTH - 1
  MAX_BYTES = (Math.log(MAX_BASE62, 16) / 2).floor

  def call
    random_decimal
      .yield_self(&Base62.method(:encode))
      .yield_self(&method(:right_pad))
  end

  def random_decimal
    SecureRandom.hex(MAX_BYTES).to_i(16)
  end

  def right_pad(str)
    str.rjust(LENGTH, '0')
  end
end
