class Shortcode::Create
  include Dry::Transaction

  step :validate
  map :generate
  try :persist, catch: [Sequel::UniqueConstraintViolation,
                        Sequel::ValidationFailed]

  private

  def validate(params)
    result = Shortcode::Contract.new.call(params)

    if result.success?
      Success(params)
    else
      Failure(result.errors)
    end
  end

  def generate(params)
    params.tap { params[:shortcode] ||= Shortcode::Generate.call }
  end

  def persist(params)
    Shortcode.create(params)
  end
end
