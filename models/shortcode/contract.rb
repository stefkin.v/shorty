class Shortcode::Contract < Dry::Validation::Contract
  params do
    required(:url).filled(:string)
    optional(:shortcode).maybe(:string)
  end

  rule(:shortcode) do
    next if value.nil? || value.match?(/^[0-9a-zA-Z_]{4,}$/)

    key.failure('invalid format')
  end
end
