class Shortcode::View
  include Dry::Transaction

  step :find
  tee :save_stats

  private

  def find(shortcode)
    model = Shortcode.find(shortcode: shortcode)

    if model
      Success(model)
    else
      Failure("not found")
    end
  end

  def save_stats(model)
    Redirect.create shortcode: model
  end
end
