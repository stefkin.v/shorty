class Shortcode::Stats::Query
  def call(shortcode)
    Redirect.where(shortcode: shortcode)
            .select(Sequel.lit('count(*)'), Sequel.lit('max(created_at)'))
            .to_a
            .first
  end
end
