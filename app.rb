require_relative 'models'

require 'roda'

class Shorty < Roda
  opts[:check_dynamic_arity] = false
  opts[:check_arity] = :warn

  plugin :json

  plugin :default_headers,
    'Content-Type'=>'application/json'

  logger = if ENV['RACK_ENV'] == 'test'
             Class.new{def write(_) end}.new
           else
             $stderr
           end
  plugin :common_logger, logger

  plugin :error_handler do |e|
    $stderr.print "#{e.class}: #{e.message}\n"
    $stderr.puts e.backtrace
    next exception_page(e, :assets=>true) if ENV['RACK_ENV'] == 'development'
    @page_title = "Internal Server Error"
    view(:content=>"")
  end

  plugin :sinatra_helpers, delegate: false
  plugin :halt

  Unreloader.require('routes'){}
end
