require_relative 'spec_helper'

describe 'POST /shorten' do
  it 'uses provided code' do
    post '/shorten', url: "https://example.com", shortcode: "aaab"
    assert_equal(last_response.status, 201)
    json = JSON.parse(last_response.body)
    assert_equal(json["shortcode"], "aaab")
    assert_equal(json.keys.size, 1)
    assert_equal(last_response.headers['Content-Type'], 'application/json')
  end

  it 'generates random code' do
    post '/shorten', url: "https://example.com"
    json = JSON.parse(last_response.body)
    assert_equal(last_response.status, 201)
    assert(!json["shortcode"].nil?)
    assert_equal(json["shortcode"].size, 6)
    assert_equal(json.keys.size, 1)
  end

  it 'responds with 400 when url is not present' do
    post '/shorten', url: ""
    assert_equal(last_response.headers['Content-Type'], 'application/json')
    assert_equal(last_response.status, 400)
  end

  it 'responds with 422 when shortcode fails to meet regexp' do
    post '/shorten', url: "https://example.com", shortcode: "abc"
    assert_equal(last_response.headers['Content-Type'], 'application/json')
    assert_equal(last_response.status, 422)
  end

  it 'responds with 409 when shortcode is already in use' do
    post '/shorten', url: "https://example.com", shortcode: "abcd"
    post '/shorten', url: "https://google.com", shortcode: "abcd"
    assert_equal(last_response.headers['Content-Type'], 'application/json')
    assert_equal(last_response.status, 409)
  end
end
