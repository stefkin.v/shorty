require_relative 'spec_helper'

describe 'GET /:shortcode/stats' do

  it 'returns 404 if there is no such code' do
    get '/foobar/stats'

    assert_equal 404, last_response.status
  end

  it 'returns 200 with start date and redirect count when there is a code' do
    created_at = Shortcode::Create.new
      .call(url: 'https://example.com', shortcode: 'foobar')
      .bind(&:created_at)

    get '/foobar/stats'

    assert_equal 200, last_response.status
    json = JSON.parse(last_response.body)
    assert_equal created_at.utc.iso8601, json['startDate']
    assert_equal 0, json['redirectCount']
    assert_equal 2, json.keys.size
    assert !json.key?('lastSeenDate')
  end

  it 'shows redirects count' do
    Shortcode::Create.new
      .call(url: 'https://example.com', shortcode: 'foobar')
      .bind(&:created_at)

    5.times { get '/foobar' }
    get '/foobar/stats'
    json = JSON.parse(last_response.body)
    assert_equal 5, json['redirectCount']
  end

  it 'keeps stats correct after they are reduced' do
    created_at = Shortcode::Create.new
      .call(url: 'https://example.com', shortcode: 'foobar')
      .bind(&:created_at)

    get '/foobar'
    get '/foobar'
    Shortcode::ReduceStats.new.call
    get '/foobar'
    get '/foobar'
    get '/foobar/stats'

    json = JSON.parse last_response.body
    assert_equal(4, json['redirectCount'])
    assert_equal(created_at.utc.iso8601, json['startDate'])
    last_redirect_at = Redirect.last.created_at.utc.iso8601
    assert_equal(last_redirect_at, json['lastSeenDate'])
  end
end
