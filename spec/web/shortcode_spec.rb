require_relative 'spec_helper'

describe 'GET /:shortcode' do
  it 'returns 404 if code has not been created before' do
    get '/foobar'
    assert_equal(last_response.status, 404)
    assert_equal(last_response.headers['Content-Type'], 'application/json')
  end

  it 'returns 302 with location equal to url' do
    Shortcode.create(url: 'https://example.com', shortcode: 'abcdef')
    get '/abcdef'
    assert_equal(302, last_response.status)
    assert_equal('https://example.com', last_response.headers['Location'])
    assert_equal(last_response.headers['Content-Type'], 'application/json')
  end
end
