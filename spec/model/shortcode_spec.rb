require_relative 'spec_helper'

describe Shortcode do
  it 'has default created_at' do
    shortcode = Shortcode.create(shortcode: 'abcd', url: 'https://example.com')
    assert !shortcode.created_at.nil?
  end

  it 'controls uniqueness of shortcodes' do
    Shortcode.create(shortcode: 'abcd', url: 'https://example.com')

    assert_raises(Sequel::ValidationFailed) do
      Shortcode.create(shortcode: 'abcd', url: 'https://github.com')
    end
  end

  describe 'Contract' do
    it 'accepts only aphanumeric chars' do
      params = { shortcode: '!!!!!', url: 'https://example.com' }
      result = Shortcode::Contract.new.call(params)

      assert_equal({ shortcode: ["invalid format"] }, result.errors.to_h)
    end

    it 'has minimal length of 4 symbols' do
      params = { shortcode: 'aaa', url: 'https://example.com' }
      result = Shortcode::Contract.new.call(params)

      assert_equal({ shortcode: ["invalid format"] }, result.errors.to_h)
    end
  end

  describe 'Generate' do
    it 'has length of 6' do
      property_of { Shortcode::Generate.call }
        .check { |code| assert_equal code.size, 6, "shortcode generate did not return string of length 6" }
    end

    it 'matches required regex of ^[0-9a-zA-Z_]{6}$' do
      property_of { Shortcode::Generate.call }
        .check { |code| assert code.match?(/^[0-9a-zA-Z_]{6}$/) }
    end

    it 'generates unique values' do
      property_of { array(100) { Shortcode::Generate.call  } }
        .check { |codes| assert_equal codes.size, codes.uniq.size }
    end
  end

  describe 'Create' do
    it 'uses provided code' do
      shortcode = Shortcode::Create.new
        .call(url: 'https://example.com', shortcode: 'aaaa')
        .bind(&:shortcode)

      assert_equal "aaaa", shortcode
    end

    it "generates random code if it's not provided" do
      shortcode = Shortcode::Create.new
        .call(url: 'https://example.com')
        .bind(&:shortcode)

      assert_equal 6, shortcode.size
    end
  end

  describe 'Stats' do
    it 'fails if code does not exist' do
      result = Shortcode::Stats.new.call("foobar")

      assert !result.success?
    end

    it 'shows stats if code exists' do
      created_at = Shortcode::Create.new.call(shortcode: "foobar", url: "https://example.com").bind(&:created_at)
      result = Shortcode::Stats.new.call("foobar")

      assert result.success?
      assert_equal(created_at.utc.iso8601, result.bind { |s| s[:startDate] })
      assert_equal(0, result.bind { |s| s[:redirectCount] })
    end

    it 'shows views and last view time' do
      created_at = Shortcode::Create.new.call(shortcode: "foobar", url: "https://example.com").bind(&:created_at)
      5.times { Shortcode::View.new.call("foobar") }
      result = Shortcode::Stats.new.call("foobar")

      assert result.success?
      assert_equal(created_at.utc.iso8601, result.bind { |s| s[:startDate] })
      assert_equal(5, result.bind { |s| s[:redirectCount] })
      assert_equal(Redirect.last.created_at.utc.iso8601, result.bind { |s| s[:lastSeenDate] })
    end

    it "squashes stats to save space" do
      code1 = Shortcode::Create.new.call(shortcode: "foobar", url: "https://example.com").bind(&:itself)
      code2 = Shortcode::Create.new.call(shortcode: "bazqux", url: "https://example.com").bind(&:itself)

      5.times { Shortcode::View.new.call("foobar") }
      10.times { Shortcode::View.new.call("bazqux") }

      Shortcode::ReduceStats.new.call

      assert_equal(code1.reload.redirects_count, 5)
      assert_equal(code2.reload.redirects_count, 10)
      assert_equal(0, Redirect.count)

      5.times { Shortcode::View.new.call("foobar") }

      Shortcode::ReduceStats.new.call
      assert_equal(code1.reload.redirects_count, 10)
      assert_equal(code2.reload.redirects_count, 10)
      assert_equal(0, Redirect.count)
    end
  end
end
