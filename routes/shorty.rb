class Shorty
  route do |r|
    r.on String, method: :get do |shortcode|
      r.is "stats" do
        Shortcode::Stats.new.call(shortcode) do |m|
          m.success(&:itself)
          m.failure { r.not_found({}) }
        end
      end

      Shortcode::View.new.call(shortcode) do |m|
        m.success { |model| r.redirect(model.url) }
        m.failure { r.not_found({}) }
      end
    end

    r.post 'shorten' do
      Shortcode::Create.new.call(url: r.params['url'],
                                 shortcode: r.params['shortcode']) do |m|
        m.success do |model|
          response.status = 201
          { shortcode: model.shortcode }
        end

        m.failure do |errors|
          r.halt(
            case errors
            when Sequel::UniqueConstraintViolation, Sequel::ValidationFailed
              409
            when ->(errs) { errs.to_h.key?(:url) }
              400
            else
              422
            end
          )
        end
      end
    end
  end
end
