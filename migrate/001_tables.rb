Sequel.migration do
  change do
    create_table(:shortcodes) do
      primary_key :id, type: :Bignum
      String :shortcode, index: { unique: true }, null: false
      String :url, null: false
      Integer :redirects_count, null: false, default: 0
      DateTime :last_redirect_at
      DateTime :created_at, null: false, default: Sequel.lit('now()')
    end

    create_table(:redirects) do
      primary_key :id, type: :Bignum
      foreign_key :shortcode_id, :shortcodes, null: false, index: true
      DateTime :created_at, null: false, default: Sequel.lit('now()')
    end
  end
end
