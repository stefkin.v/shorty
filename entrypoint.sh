#!/bin/sh
set -e

until psql -h "${RACK_ENV}_db" -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"

echo "Migrating db"

rake "${RACK_ENV}_up"

whenever --update-crontab

exec "$@"
