begin
  require_relative '.env.rb'
rescue LoadError
end

require 'sequel/core'

# Delete APP_DATABASE_URL from the environment, so it isn't accidently
# passed to subprocesses.  APP_DATABASE_URL may contain passwords.

DB = Sequel.connect(adapter: 'postgres', host: ENV['SHORTY_DATABASE_HOST'],
                    database: ENV['SHORTY_DATABASE_NAME'], user: 'postgres')
