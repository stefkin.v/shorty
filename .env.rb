case ENV['RACK_ENV'] ||= 'development'
when 'test'
  ENV['SHORTY_SESSION_SECRET'] ||= "rnCUZ8121pt619uq+k3ejUSjMiaGYq95TzyE+UXdtGICFjG4QX6PwnePca4l\nUXoNzmnXmxv44+fwCW9aNWEvrA==\n".unpack('m')[0]
  ENV['SHORTY_DATABASE_NAME'] ||= "shorty_test"
  ENV['SHORTY_DATABASE_HOST'] ||= "test_db"
when 'production'
  ENV['SHORTY_SESSION_SECRET'] ||= "CeixdAmXSVDk7KaOuvJrHFIKo+iPqKq3RaKHDNc/kf+/jdhTMfKhnNiCBY8S\nLPath+EqUa3NR8o2j1Iy5jgdoA==\n".unpack('m')[0]
  ENV['SHORTY_DATABASE_NAME'] ||= "shorty_production"
  ENV['SHORTY_DATABASE_HOST'] ||= "production_db"
else
  ENV['SHORTY_SESSION_SECRET'] ||= "oU4zeSwPP+0TQo4dUJ1/Asu/ym8/jLKGNlIa8DetEMIl3jQfP7BBweg8d9is\n7+c+DxFu339dCg45us9VDhIEug==\n".unpack('m')[0]
  ENV['SHORTY_DATABASE_NAME'] ||= "shorty_development"
  ENV['SHORTY_DATABASE_HOST'] ||= "localhost"
end
